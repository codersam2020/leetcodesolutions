class Solution {
    public int maxArea(int[] maxArea) {
        if(maxArea.length==2)return Math.min(maxArea[1],maxArea[0]);
        int pre[]=new int[maxArea.length];
        int maxpre=0;
        for(int i=1;i<maxArea.length;i++){
            pre[i]=0;
            for(int j=0;j<i;j++){
                if(maxArea[j]>=maxArea[i]){
                    pre[i]=Math.abs(i-j)*maxArea[i];
                    maxpre=Math.max(maxpre,pre[i]);
                    break;
                }
                
            }
        }
        int post[]=new int[maxArea.length];
        int maxpost=0;
        for(int i=0;i<maxArea.length-1;i++){
            post[i]=0;
            for(int j=maxArea.length-1;j>i;j--){
                if(maxArea[j]>=maxArea[i]){
                    post[i]=Math.abs(i-j)*maxArea[i];
                    maxpost=Math.max(maxpost,post[i]);
                    break;
                }
                
            }
        }
        
        return Math.max(maxpre,maxpost);
    }
}
