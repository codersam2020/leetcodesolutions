class Solution {
    public int[] productExceptSelf(int[] nums) {
        int pre[]=new int[nums.length];
        for(int i=0;i<nums.length;i++){
            if(i==0)pre[i]=nums[i];
            else{
                pre[i]=pre[i-1]*nums[i];
            }
        }
        int post[]=new int[nums.length];
        for(int i=nums.length-1;i>=0;i--){
            if(i==nums.length-1)post[i]=nums[i];
            else{
                post[i]=post[i+1]*nums[i];
            }
        }
        int a[]=new int[nums.length];
        for(int i=0;i<nums.length;i++){
            if(i==0){
                a[i]=post[i+1];
                continue;
                }
            if(i==nums.length-1){
                a[i]=pre[i-1];
                continue;
                }
            a[i]=pre[i-1]*post[i+1];
        }
        return a;
    }
}
